# tekartik_gitlab_ci.dart

Travis helper scripts to allow running unit test on Chrome

## Setup

This assumes you are familiar with Dart and Travis integration

Include `tekartik_gitlab_ci.dart` as a development dependencies in your `pubspec.yaml` file

```yaml
dev_dependencies:
  test: any
  tekartik_gitlab_ci:
    git:
      url: git@gitlab.com:tekartik_public/tekartik_gitlab_ci.dart
      ref: dart2
```

Create the following `.travis.yml` file

```yaml
image: google/dart:latest

stages:
- test

before_script:
  - export PUB_CACHE=$PWD/.pub_cache/
  - pub version
  - pub get
  - source $(pub run tekartik_gitlab_ci:env_rc)

test:
  stage: test
  script:
    - pub run test -p vm,chrome
```

## Example

This project use the solution itself