#!/usr/bin/env bash

set -xe

sudo add-apt-repository ppa:ubuntu-toolchain-r/test -y
sudo apt-get update -qq -y
sudo apt-get install libstdc++6 -y -qq