#!/usr/bin/env bash

set -xe

# for debugging
pub run tekartik_gitlab_ci:show_env

# Test on purpose separately to see which platform fails
pub run test -p vm -r expanded
pub run test -p chrome -r expanded
